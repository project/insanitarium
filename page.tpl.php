<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
  </head>
  <body>
  	
<div class="container">
	<div id="header">
		<div class="logo">     
		  <?php if (!empty($logo)): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        <span id="site-name"><?php print $site_name ?></span></a>
     <?php endif; ?>
    </div>
    <div id="login"><?php print $login ?></div>
		<div class="secondary-nav"><?php print theme('links', $secondary_links, array('class' => 'links secondary-links')); ?></div>	
		    <div class="nav"><?php $menu_name = variable_get('menu_primary_links_source', 'primary-links');
print menu_tree($menu_name); ?>
		<div class="search"><?php print $search_box; ?></div></div>
	</div><!-- end of header -->
</div>	
<div id="nav">

</div><!-- end of nav -->

<div class="container">
	<div id="showcase"><img src="<?php print base_path() . path_to_theme() ?>/images/showcase.jpg" /></div><!-- end of showcase -->
	
	<div id="content-wrapper">
		<div id="leftside">
		<?php if($left || $right): ?>
			<div id="left">
				<?php print $left; ?>
			</div><!-- end of left inside leftside -->
			<div id="right">
				<?php print $right; ?>
			</div><!-- end of right inside lefside -->
			<div class="clear"></div>
		<?php endif; ?>
		<div id="content">
		  <div class="tabs"><?php print $tabs ?><?php print $tabs2 ?></div>
			<?php print $content ?>
		</div><!-- end of content -->
		</div><!-- end of lefside -->
		<div id="rightside">
			<?php print $rightside ?>
		</div><!-- end of rightside -->
		<div class="clear"></div>
	</div><!-- end of content-wrapper -->
	<div id="footer">
<?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
		<p class="legal"><?php print $footer_message ?></p>
	</div><!-- end of footer -->
</div>
</div>
</body>
</html>
